FROM node:12.13.0

WORKDIR /usr/src/app

COPY package*.json ./usr/src/app


RUN npm install

COPY . /usr/src/app

ENV PORT 5000
EXPOSE $PORT
CMD [ "npm", "start" ]